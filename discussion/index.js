// console.log("Hello 157!");

console.log(document.querySelector("#txt-first-name"));
// query selector is used for creating specified 

console.log(document)
// document refers to the whole page
// querySelector is used to select a specific element
// #(hash key) use to target an id inside a page with the name


console.log(document.getElementById("txt-first-name"))
/*
	ALternative:
	
	document.getElementById("txt-first-name")
	document.getElementByClassName()
	document.getElementByTagName()


*/

const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");

// Event Listeners
/*
	selectedElement.addEventListener('event', function);

*/

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
});


/*
function printFirstName(event) {
	spanFullName.innerHTML = txtFirstName.value;
}

txtFirstName.addEventListener('keyup', printFirstName)*/

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target)
	console.log(event.target.value)
})